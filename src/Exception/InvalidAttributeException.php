<?php

namespace Samtt\Exception;

/**
 * Class InvalidAttributeException
 *
 * @author Danniel Magno
 * @package Samtt\Exception
 */
class InvalidAttributeException extends \Exception
{
    /**
     * @param   string  $service The invalid attribute tried to be accessed
     */
    public function __construct($service)
    {
        parent::__construct(sprintf('Invalid attribute \'%s\'', $service));
    }
}
