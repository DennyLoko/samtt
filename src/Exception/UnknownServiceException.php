<?php

namespace Samtt\Exception;

/**
 * Class UnknownServiceException
 *
 * @author Danniel Magno
 * @package Samtt\Exception
 */
class UnknownServiceException extends \Exception
{
    /**
     * @param string $service The unknown service being called
     */
    public function __construct($service)
    {
        parent::__construct(sprintf('Unknown service \'%s\'.', $service));
    }
}
