<?php

namespace Samtt\Controller;

interface Controller
{
    /**
     * Dispatch the content of the page
     *
     * @return string
     */
    public function dispatch();
}
