<?php

namespace Samtt\Controller;

use Samtt\Entity\Message;
use Pheanstalk\Exception;

class IndexController extends GenericController
{
    /* (non-phpdoc)
     * @see ControllerInterface::dispatch()
     */
    function dispatch()
    {
        // Creates a new message from the request
        $message = Message::createFromArray($_REQUEST);

        // Set the default header
        header('Content-type: application/json');

        // Try to insert the job on the queue
        try {
            // Inserts the message into the queue
            $this->beanstalkd->useQueue('samtt')->createJob(serialize($message));

            // Returns the success
            echo '{"status": "ok"}' . "\n";
        } catch (Exception $e) {
            // Change the HTTP error code
            http_response_code(500);

            // Returns the error
            echo '{"status": "fail", "message": "' . $e->getMessage() . '"}' . "\n";
        }
    }
}
