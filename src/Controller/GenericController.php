<?php

namespace Samtt\Controller;

use Samtt\Service\Cache\Cache;
use Samtt\Service\Cache\MemcachedService;
use Samtt\Service\ConfigService;
use Samtt\Service\Queue\BeanstalkdService;
use Samtt\Service\Queue\Queue;

/**
 * Class GenericController
 *
 * @author Danniel Magno
 * @package Samtt\Controller
 *
 * @property    Queue           beanstalkd
 * @property    ConfigService   config
 * @property    Cache           memcached
 */
abstract class GenericController implements Controller
{
    use \DependencyInjectionTrait;

    /**
     * @return Queue
     */
    protected function create_beanstalkd()
    {
        return new BeanstalkdService();
    }

    protected function create_memcached()
    {
        return new MemcachedService();
    }
}
