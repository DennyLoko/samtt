<?php

namespace Samtt\Controller;

class StatsController extends GenericController
{
    public function dispatch()
    {
        // Set the default header
        header('Content-type: application/json');

        // Preparing the return values
        $count_15m_ago = $this->memcached->get('last_15_min_mo_count');

        if ($count_15m_ago === false) {
            $count_15m_ago = 0;
        }

        $time_span = $this->memcached->get('time_span_last_10k');

        if ($time_span === false) {
            $time_span = [ '0000-00-00 00:00:00', '0000-00-00 00:00:00' ];
        }

        // Print the data
        echo json_encode([
            'last_15_min_mo_count'  => $count_15m_ago,
            'time_span_last_10k'    => $time_span,
        ]);
    }
}
