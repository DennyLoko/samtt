<?php
require __DIR__ . '/../../vendor/autoload.php';

use Samtt\Service\Queue\BeanstalkdService;

$beanstalk = new BeanstalkdService();

echo $beanstalk->queueLength('samtt') . "\n";
