<?php
require __DIR__ . '/../../vendor/autoload.php';

use Samtt\Service\Queue\BeanstalkdService;

// Handle terminate signals
declare(ticks = 1);
$terminate = false;
pcntl_signal(SIGTERM, function() {
    global $terminate;
    $terminate = true;

    echo "Terminating...\n";
});

$beanstalk = new BeanstalkdService();

while ($terminate === false && $beanstalk->queueLength('samtt') > 0) {
    $job = $beanstalk->watchQueue('samtt')->getJob();

    if ($job) {
        echo "Deleting job #{$job->getId()}\n";
        $beanstalk->deleteJob($job);
    }

    usleep(500);
}
