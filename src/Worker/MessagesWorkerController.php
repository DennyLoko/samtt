<?php
namespace Samtt\Worker;

require __DIR__ . '/../../vendor/autoload.php';

class MessagesWorkerController
{
    use \DependencyInjectionTrait;

    private $workersCount = 0;

    public function __construct()
    {
        $config = $this->config->get('messagesworkercontroller');
        $this->workersCount = $config['processCount'];
    }

    public function manageProcesses()
    {
        // Check if we have the minimum number of process running
        if ($this->getProcessesCurrentlyRunning() < $this->workersCount) {
            $processToStart = $this->workersCount - $this->getProcessesCurrentlyRunning();

            for ($i = 0; $i < $processToStart; $i++) {
                $this->startProcess();
            }
        }
    }

    public function startProcess()
    {
        shell_exec('nohup php ' . __DIR__ . '/MessagesWorker.php > /dev/null &');
        echo 'New MessagesWorker started.' . "\n";
    }

    public function getProcessesCurrentlyRunning()
    {
        $count = (int) shell_exec("ps aux | grep '[M]essagesWorker.php' | awk '{ print \$2 }' | wc -l");
        return $count;
    }
}

$mwc = new MessagesWorkerController();
$mwc->manageProcesses();
