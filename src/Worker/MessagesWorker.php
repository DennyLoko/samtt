<?php
require __DIR__ . '/../../vendor/autoload.php';

use Samtt\Entity\Message;
use Samtt\Service\Database\MysqlService;
use Samtt\Service\Queue\BeanstalkdService;

// Handle terminate signals
declare(ticks = 1);
$terminate = false;
pcntl_signal(SIGTERM, function() {
    global $terminate;
    $terminate = true;

    echo "Terminating...\n";
});

// Instantiate the used objects
$beanstalk  = new BeanstalkdService();
$mysql      = new MysqlService();

// Runs indefinitely
while ($terminate === false) {
    try {
        // Gets the job
        $job = $beanstalk->watchQueue('samtt')->getJob();

        // We have a job?
        if ($job) {
            echo "Processing job #{$job->getId()}\n";

            // Get the message
            /* @var $message Message */
            $message = unserialize($job->getData());

            // Get the token for the message
            $token = shell_exec(__DIR__ . '/../../code/web/registermo ' . $message->toJson());
            $message->setToken($token);

            // Save the object into database
            $mysql->insertObject($message);

            // Remove the job from the queue
            $beanstalk->deleteJob($job);
        }
    } catch (Exception $e) {
        error_log('The following error occurred on MessagesWorker.php. Error: ' . $e->getMessage());
    }

    usleep(500);
}
