<?php
require __DIR__ . '/../../vendor/autoload.php';

use Samtt\Service\Cache\MemcachedService;
use Samtt\Service\Database\MysqlService;

$mysql      = new MysqlService();
$memcached  = new MemcachedService();

// 15m ago stats
$date_15m_go = new \DateTime('15 minutes ago');

$stmt_15m_ago = $mysql->prepare('SELECT count(*) FROM `mo` WHERE `created_at` > :date');
$stmt_15m_ago->bindValue(':date', $date_15m_go->format('Y-m-d H:i:s'));
$stmt_15m_ago->execute();

$memcached->set('last_15_min_mo_count', (int) $stmt_15m_ago->fetchColumn(0));

// Time span stats
$stmt_time_span = $mysql->query('SELECT min(received_at), max(received_at) FROM `mo` ORDER BY `id` DESC LIMIT 10000');
$stmt_time_span->execute();

$memcached->set('time_span_last_10k', $stmt_time_span->fetch(PDO::FETCH_NUM));
