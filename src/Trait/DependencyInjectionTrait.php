<?php

use Samtt\Exception\UnknownServiceException;
use Samtt\Service\ConfigService;

/**
 * This trait implements the basic functionality of an DI.
 * Based on http://dqxtech.net/blog/2014-06-13/simple-do-it-yourself-php-service-container
 */
trait DependencyInjectionTrait
{
    /**
     * @var array
     */
    private $services = [];

    /**
     * Magic method that is triggered when someone calls $container->$name.
     *
     * @param   string  $name The machine name of the service.
     *
     * @return object
     * @throws UnknownServiceException
     */
    public function __get($name)
    {
        return isset($this->services[$name]) ?
            $this->services[$name] : ($this->services[$name] = $this->createService($name));
    }

    /**
     * @param   string  $name
     *
     * @return object The service.
     * @throws UnknownServiceException
     */
    private function createService($name)
    {
        // Method to be implemented in a subclass.
        $method = 'create_' . $name;

        if (!method_exists($this, $method)) {
            throw new UnknownServiceException($name);
        }

        return $this->$method();
    }

    /**
     * @return ConfigService
     */
    protected function create_config()
    {
        return new ConfigService();
    }
}
