<?php

namespace Samtt\Service\Cache;

interface Cache
{
    /**
     * Removes a data from the cache
     *
     * @param string $key
     * @return mixed
     */
    public function delete($key);

    /**
     * Gets a data from the cache
     *
     * @param string $key
     * @return mixed
     */
    public function get($key);

    /**
     * Inserts or updates a data in the cache
     *
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return mixed
     */
    public function set($key, $value, $ttl = 0);
}
