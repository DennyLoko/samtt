<?php

namespace Samtt\Service\Cache;

use \Memcached;
use Samtt\Service\ConfigService;
use Samtt\Exception\InvalidAttributeException;

/**
 * Class MemcachedService
 *
 * @package Samtt\Service
 * @property ConfigService config
 */
class MemcachedService extends Memcached implements Cache
{
    // Traits used on this class
    use \DependencyInjectionTrait;

    /**
     * Define our custom constructor to set our defaults
     *
     * @throws InvalidAttributeException
     */
    public function __construct()
    {
        // Create an persistent connection to memcached
        parent::__construct('samtt');
        $this->setOption(Memcached::OPT_LIBKETAMA_COMPATIBLE, true);

        // Prevents duplicated servers
        if (!count($this->getServerList())) {
            $memcachedConfig = $this->config->get('memcached');
            $this->addServer($memcachedConfig['host'], $memcachedConfig['port']);
        }
    }
}
