<?php

namespace Samtt\Service\Queue;

interface Queue
{
    /**
     * Enqueue a job
     *
     * @param   string  $data
     * @param   int     $priority
     * @param   int     $delay
     * @param   int     $ttr
     *
     * @return  int
     */
    public function createJob($data, $priority = 1024, $delay = 0, $ttr = 60);

    /**
     * Removes a job from the queue
     *
     * @param   object  $job
     *
     * @return  $this
     */
    public function deleteJob($job);

    /**
     * Gets a job from the queue
     *
     * @return  object
     */
    public function getJob();

    /**
     * Selects a queue to use
     *
     * @param   string  $queue
     *
     * @return  $this
     */
    public function useQueue($queue);

    /**
     * Adds a queue to the watch list
     *
     * @param   string  $queue
     *
     * @return $this
     */
    public function watchQueue($queue);

    /**
     * Removes a queue from the watch list
     *
     * @param   string  $queue
     *
     * @return $this
     */
    public function ignoreQueue($queue);

    /**
     * Returns the length of the queue
     *
     * @param   string  $queue
     *
     * @return int
     */
    public function queueLength($queue);
}
