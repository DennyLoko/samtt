<?php

namespace Samtt\Service\Queue;

use Pheanstalk\Job;
use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;
use Pheanstalk\Response;
use Samtt\Service\ConfigService;

/**
 * Class BeanstalkdService
 *
 * @package Samtt\Service\Queue
 * @property ConfigService config
 */
class BeanstalkdService extends Pheanstalk implements Queue
{
    use \DependencyInjectionTrait;

    public function __construct()
    {
        // Gets the config of beanstalkd
        $beanstalkd = $this->config->get('beanstalkd');

        // Passes the beanstalkd config to Pheanstalk library
        parent::__construct($beanstalkd['host'], $beanstalkd['port'], $beanstalkd['timeout'], $beanstalkd['persistent']);
    }

    /**
     * @param string $data
     * @param int    $priority
     * @param int    $delay
     * @param int    $ttr
     *
     * @return int
     */
    public function createJob($data, $priority = PheanstalkInterface::DEFAULT_PRIORITY, $delay = PheanstalkInterface::DEFAULT_DELAY, $ttr = PheanstalkInterface::DEFAULT_TTR)
    {
        return $this->put($data, $priority, $delay, $ttr);
    }

    /**
     * @param object $job
     *
     * @return $this
     */
    public function deleteJob($job)
    {
        return $this->delete($job);
    }

    /**
     * @return bool|Job
     */
    public function getJob()
    {
        return $this->reserve();
    }

    /**
     * @param string $queue
     *
     * @return $this
     */
    public function useQueue($queue)
    {
        return $this->useTube($queue);
    }

    /**
     * @param string $queue
     *
     * @return $this
     */
    public function watchQueue($queue)
    {
        return $this->watch($queue);
    }

    /**
     * @param string $queue
     *
     * @return $this
     */
    public function ignoreQueue($queue)
    {
        return $this->ignore($queue);
    }

    /**
     * @param string $queue
     *
     * @return integer
     */
    public function queueLength($queue)
    {
        return (int) $this->statsTube($queue)->current_jobs_ready;
    }
}
