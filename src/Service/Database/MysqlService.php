<?php

namespace Samtt\Service\Database;

use Samtt\Entity\Entity;

class MysqlService extends \PDO implements Database
{
    use \DependencyInjectionTrait;

    private $connection;

    public function __construct()
    {
        // Get MySQL config
        $config = $this->config->get('mysql');

        // Create the PDO object
        parent::__construct('mysql:host=' . $config['host'] . ';dbname=' . $config['database'],
                            $config['user'],
                            $config['pass']
        );

        // Set the error mode
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Inserts an object into the database
     *
     * @param   Entity  $obj
     *
     * @return bool
     */
    public function insertObject(Entity $obj)
    {
        // Get the columns
        $columns = $obj->getColumns();

        // Generate the SQL
        $sql  = 'INSERT INTO `' . $obj->getTable() . '` (`' . implode('`, `', array_keys($columns)) . '`) ';
        $sql .= 'VALUES (:' . implode(', :', array_keys($columns)) . ');';

        // Prepare the query
        $stmt = $this->prepare($sql);

        // Iterate over the values
        foreach ($columns as $column => $value) {
            if ($value instanceof \DateTime) {
                $stmt->bindValue(':' . $column, $value->format('Y-m-d H:i:s'));
            } else {
                $stmt->bindValue(':' . $column, $value);
            }
        }

        // Executes the statement
        return $stmt->execute();
    }
}
