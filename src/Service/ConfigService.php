<?php

namespace Samtt\Service;

use Samtt\Exception\InvalidAttributeException;

/**
 * Class responsible to get the config data from the config.php file
 *
 * @package Samtt\Service
 */
class ConfigService
{
    // Traits used on this class
    use \DependencyInjectionTrait;

    /**
     * Read the config.php file and generate the attributes of the classed based on it
     */
    public function __construct($config = null)
    {
        // Include the config file
        if (is_string($config)) {
            $configArray = include $config;
        } elseif (is_array($config)) {
            $configArray = $config;
        } else {
            $configArray = include __DIR__ . '/../config.php';
        }

        // Iterate over the keys from the config file
        foreach ($configArray as $key => $value) {
            // Set the values from the config file into the attributes of the class
            $this->{$key} = $value;
        }
    }

    /**
     * @param string $attribute The attribute we're looking for
     *
     * @return object
     * @throws InvalidAttributeException
     */
    public function get($attribute)
    {
        if (isset($this->{$attribute})) {
            return $this->{$attribute};
        }

        throw new InvalidAttributeException($attribute);
    }
}
