<?php

namespace Samtt\Entity;

interface Entity
{
    /**
     * @return string
     */
    public function getTable();

    /**
     * @return array
     */
    public function getColumns();
}
