<?php

namespace Samtt\Entity;

class Message implements Entity
{
    /**
     * @var string
     */
    private $auth_token = '';

    /**
     * @var \DateTime
     */
    private $received_at;

    /**
     * @var string
     */
    private $msisdn = '';

    /**
     * @var int
     */
    private $operatorId = 0;

    /**
     * @var int
     */
    private $shortCodeId = 0;

    /**
     * @var string
     */
    private $text = '';

    /**
     * @param   string  $msisdn
     * @param   int     $operatorId
     * @param   int     $shortCodeId
     * @param   string  $text
     */
    public function __construct($msisdn, $operatorId, $shortCodeId, $text)
    {
        $this->received_at = new \DateTime();
        $this->msisdn = $msisdn;
        $this->operatorId = (int) $operatorId;
        $this->shortCodeId = (int) $shortCodeId;
        $this->text = $text;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->received_at;
    }

    /**
     * @return string
     */
    public function getMsisdn()
    {
        return $this->msisdn;
    }

    /**
     * @return int
     */
    public function getOperatorId()
    {
        return $this->operatorId;
    }

    /**
     * @return int
     */
    public function getShortCodeId()
    {
        return $this->shortCodeId;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param   string  $token
     *
     * @return $this
     */
    public function setToken($token)
    {
        $this->auth_token = $token;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->auth_token;
    }

    /**
     * Return the attributes of the object in JSON format
     *
     * @param   bool    $includeToken
     *
     * @return string
     */
    public function toJson($includeToken = false)
    {
        $returnArr = [
            'msisdn'        => $this->getMsisdn(),
            'operatorid'    => $this->getOperatorId(),
            'shortcodeid'   => $this->getShortCodeId(),
            'text'          => $this->getText()
        ];

        if ($includeToken === true) {
            $returnArr['auth_token'] = $this->getToken();
        }

        return json_encode($returnArr);
    }

    public function getTable()
    {
        return 'mo';
    }

    public function getColumns()
    {
        $columns = get_object_vars($this);
        $columns['created_at'] = new \DateTime();

        return $columns;
    }

    /**
     * @param   array   $values
     *
     * @return Message
     */
    static function createFromArray($values)
    {
        return new Message(
            $values['msisdn'],
            $values['operatorid'],
            $values['shortcodeid'],
            $values['text']
        );
    }
}
