<?php
require __DIR__ . '/../../vendor/autoload.php';

use Samtt\Controller\StatsController;

$page = new StatsController();
$page->dispatch();
