<?php
require __DIR__ . '/../../vendor/autoload.php';

use Samtt\Controller\IndexController;

$page = new IndexController();
$page->dispatch();
