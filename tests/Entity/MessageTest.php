<?php

namespace Samtt\Entity;

class MessageTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Message
     */
    private $message;

    public function setUp()
    {
        $this->message = new Message(
            '60123456789',
            3,
            8,
            'ON GAMES'
        );
    }

    public function testConstructor()
    {
        $this->assertEquals('60123456789', $this->message->getMsisdn());
        $this->assertEquals(3, $this->message->getOperatorId());
        $this->assertEquals(8, $this->message->getShortCodeId());
        $this->assertEquals('ON GAMES', $this->message->getText());
        $this->assertInstanceOf('\DateTime', $this->message->getDate());
    }

    public function testCreateFromArray()
    {
        $message = Message::createFromArray([
            'msisdn'        => '60123456789',
            'operatorid'    => 3,
            'shortcodeid'   => 8,
            'text'          => 'ON GAMES'
        ]);

        $this->assertInstanceOf('Samtt\Entity\Message', $message);
    }

    public function testSetToken()
    {
        $token = md5(time());
        $this->message->setToken($token);

        $this->assertEquals($token, $this->message->getToken());
    }

    public function testToJson()
    {
        json_decode($this->message->toJson(true));

        $this->assertEquals(JSON_ERROR_NONE, json_last_error());
    }
}
