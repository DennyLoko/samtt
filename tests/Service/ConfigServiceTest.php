<?php

namespace Samtt\Service;

class ConfigServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException Samtt\Exception\InvalidAttributeException
     */
    public function testInvalidAttributeException()
    {
        $config = new ConfigService();
        $config->get('fakeConfigToGenerateException');
    }

    public function testValidConfig()
    {
        $config = new ConfigService([
            'mysql' => [
                'host'  => '127.0.0.1'
            ]
        ]);

        $this->assertInternalType('array', $config->get('mysql'));
    }

    public function testLoadFromCustomFile()
    {
        $config = new ConfigService(__DIR__ . '/../Resources/config.php');

        $this->assertEquals('value', $config->get('unit_test')['key']);
    }
}
