<?php

namespace Samtt\Service\Queue;

class BeanstalkdServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var BeanstalkdService
     */
    private $beanstalkd;

    private $queue = 'testQueue';

    public function setUp()
    {
        $this->beanstalkd = new BeanstalkdService();
    }

    public function testConfig()
    {
        $this->assertInstanceOf('Samtt\Service\ConfigService', $this->beanstalkd->config);
        $this->assertInternalType('array', $this->beanstalkd->config->get('beanstalkd'));
    }

    public function testConnection()
    {
        $this->assertInstanceOf('Pheanstalk\Response\ArrayResponse', $this->beanstalkd->stats());
    }

    public function testUseQueue()
    {
        $this->beanstalkd->useQueue($this->queue);
        $this->assertEquals($this->queue, $this->beanstalkd->listTubeUsed());
    }

    /**
     * @depends testUseQueue
     */
    public function testCreateJob()
    {
        $this->assertInternalType('int', $this->beanstalkd->createJob('test'));
    }

    /**
     * @depends testCreateJob
     */
    public function testQueueLength()
    {
        $this->assertInternalType('int', $this->beanstalkd->queueLength($this->queue));
        $this->assertGreaterThan(0, $this->beanstalkd->queueLength($this->queue));
    }

    /**
     * @depends testCreateJob
     */
    public function testWatchQueue()
    {
        $this->assertInstanceOf('Samtt\Service\Queue\BeanstalkdService', $this->beanstalkd->watchQueue($this->queue));
        $this->assertContains($this->queue, $this->beanstalkd->listTubesWatched());
    }

    /**
     * @depends testWatchQueue
     */
    public function testGetJob()
    {
        $job = $this->beanstalkd->getJob();
        $this->beanstalkd->release($job);

        $this->assertInstanceOf('Pheanstalk\Job', $job);
    }

    /**
     * @depends testGetJob
     */
    public function testDeleteJob()
    {
        $job = $this->beanstalkd->getJob();

        $this->assertInstanceOf('Samtt\Service\Queue\BeanstalkdService', $this->beanstalkd->deleteJob($job));
        $this->assertEquals(0, $this->beanstalkd->queueLength($this->queue));
    }

    /**
     * @depends testWatchQueue
     */
    public function testIgnoreQueue()
    {
        $this->beanstalkd->ignoreQueue($this->queue);
        $this->assertNotContains($this->queue, $this->beanstalkd->listTubesWatched());
    }
}
