<?php

namespace Samtt\Service\Cache;

class MemcachedServiceTest extends \PHPUnit_Framework_TestCase
{
    private $memcached;

    public function setUp()
    {
        $this->memcached = new MemcachedService();
    }

    public function testConfig()
    {
        $this->assertInstanceOf('Samtt\Service\ConfigService', $this->memcached->config);
        $this->assertInternalType('array', $this->memcached->config->get('memcached'));
    }

    public function testConnection()
    {
        $this->assertEquals(true, $this->memcached->set('test', 'phpunit'));
        $this->assertEquals('phpunit', $this->memcached->get('test'));
        $this->assertEquals(true, $this->memcached->delete('test'));
    }
}
