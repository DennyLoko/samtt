<?php

namespace Samtt\Service\Database;

use Samtt\Entity\Message;

class MysqlServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var MysqlService
     */
    private $mysql;

    public function setUp()
    {
        $this->mysql = new MysqlService();
    }

    public function testConfig()
    {
        $this->assertInstanceOf('Samtt\Service\ConfigService', $this->mysql->config);
        $this->assertInternalType('array', $this->mysql->config->get('mysql'));
    }

    public function testConnection()
    {
        $this->assertEquals(4, $this->mysql->query('SELECT 2+2')->fetchColumn(0));
    }

    public function testSaveObject()
    {
        $this->mysql->insertObject(new Message(
            '55123456789',
            3,
            8,
            'PHPUnit Test'
        ));

        $this->assertGreaterThan(0, $this->mysql->exec('DELETE FROM `mo`WHERE `text`=\'PHPUNit Test\''));
    }
}
