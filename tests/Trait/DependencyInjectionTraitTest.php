<?php

class DependencyInjectionTraitTest extends PHPUnit_Framework_TestCase
{
    public function testCreateService()
    {
        $mock = $this->getMockForTrait('DependencyInjectionTrait');
        $this->assertInstanceOf('Samtt\Service\ConfigService', $mock->config);
    }

    /**
     * @expectedException Samtt\Exception\UnknownServiceException
     */
    public function testUnknownServiceException()
    {
        $mock = $this->getMockForTrait('DependencyInjectionTrait');
        $mock->invalidService;
    }
}
